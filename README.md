![N|Solid](logo_ioasys.png)

# Desafio Pessoa Desenvolvedora iOS

## Informações do desenvolvedor sobre o projeto

* Não é necessário nenhuma instrução específica para rodar o projeto;
* O projeto foi modularizado separando as partes agnosticas de plataforma (modelos, comunicação, view model). Esses objetos estão em frameworks de target macOS para aproveitar a maior efienciência ao rodar testes unitários, uma vez que não é necessário rodar simulador nos testes;
* O projeto foi construido pensando no desacoplamento dos módulos e objetos para facilitar os testes, usando Mocks e Spys;
* Para checar a cobertura total dos testes é necessário rodar os testes no esquema "ioasys-app", pois é o único que possui todos os targets;
* Foi optado o uso do Keychain para armazenar a sessão, tendo em vista que são informações sensíveis;

## 🏗  O que fazer?
Você deve fazer um fork deste repositório, criar o código e ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por e-mail o resultado do seu teste. 

## 📱Escopo de projeto
Deve ser criado um aplicativo iOS utilizando Swift com as seguintes especificações:

* Login e acesso de Usuário já registrado
    * Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
    * Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
* Busca de Empresas
* Listagem de Empresas
* Detalhamento de Empresas

**Sinta-se a vontade para:**

* Escolher a arquitetura
* Usar ou não usar bibliotecas
* Estruturar seu layout com storyboards, xibs, view code (UIKit ou SwiftUI).

## 🕵 Itens a serem avaliados
Pense no desafio como uma oportunidade de mostrar todo o seu conhecimento. Independente de onde conseguiu chegar no teste, é importante disponibilizar sua implementação para analisarmos.

* Estrutura do projeto
* Consumo de APIs
* Lógicas de busca
* Estruturação de layout e fluxo de aplicação
* Utilização de código limpo e princípios SOLID
* Boas práticas da linguagem

## 🎁 Extra
Estes itens não são obrigatórios, porém desejados.

* Testes unitários
* Testes de UI
* Modularização

## 🚨 Informações Importantes
* Layout e recortes disponíveis no Figma (http://bit.ly/testeios)
* Você deve fazer um cadastro no Figma para ter acesso ao layout.
* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.

## Dados para Teste
* Servidor: https://empresas.ioasys.com.br/api
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234