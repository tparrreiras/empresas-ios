//
//  RequestLoginFromRemoteUseCaseTests.swift
//  LoginTests
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import XCTest

import Login

class RequestLoginFromRemoteUseCaseTests: XCTestCase {
    
    func test_init_doesNotRequestDataFromURL() {
        let (_, client) = self.makeSUT()
        
        XCTAssertTrue(client.requestedURLs.isEmpty)
    }
    
    func test_requestLogin_requestDataFromURL() {
        let url = URL(string: "https://a-given-url.com")!
        let (sut, client) = self.makeSUT(url: url)
        sut.request(withData: Data()) { _ in }
        
        XCTAssertEqual(client.requestedURLs, [url])
    }
    
    func test_requestLoginTwice_requestsDataFromURLTwice() {
        let url = URL(string: "https://a-given-url.com")!
        let (sut, client) = self.makeSUT(url: url)
        sut.request(withData: Data()) { _ in }
        sut.request(withData: Data()) { _ in }
        
        XCTAssertEqual(client.requestedURLs, [url, url])
    }
    
    func test_requestLogin_deliversErrorOnClientError() {
        let (sut, client) = makeSUT()
        
        self.expect(sut, toCompleteWith: failure(.connectivity), when: {
            let clientError = NSError(domain: "Test", code: 0)
            client.complete(with: clientError)
        })
    }
    
    func test_requestLogin_deliversErrorOnNon200HTTPResponse() {
        let (sut, client) = makeSUT()
        
        let samples = [199, 300, 400, 500]
        samples.enumerated().forEach { index, code in
            self.expect(sut, toCompleteWith: failure(.invalidData), when: {
                client.complete(withStatusCode: code, at: index)
            })
        }
    }
    
    func test_requestLogin_deliversErrorOn200HTTPResponseWithInvalidJSON() {
        let (sut, client) = makeSUT()
        
        
        self.expect(sut, toCompleteWith: failure(.invalidData), when: {
            let invalidJSON = [String: String]()
            client.complete(withStatusCode: 200, data: invalidJSON)
        })
    }
    
    func test_requestLogin_deliversItemOn200HTTPResponseWithJSONItem() {
        let (sut, client) = makeSUT()
        
        let item = LoginHeaders(accessToken: "a token", client: "a client", uid: "a id")
        
        self.expect(sut, toCompleteWith: .success(item), when: {
            client.complete(withStatusCode: 200, data: makeItemJSON(item))
        })
    }
    
    func test_requestLogin_doesNotDeliversResultAfterSUTInstanceHasBeenDeallocated() {
        let url = URL(string: "https://any-url.com")!
        let client = HTTPClientSpy()
        var sut: RemoteLoginRequester? = RemoteLoginRequester(url: url, client: client)
        
        let item = LoginHeaders(accessToken: "a token", client: "a client", uid: "a id")
        
        var capturedResults = [RemoteLoginRequester.Result]()
        sut?.request(withData: Data()) { capturedResults.append($0) }
        
        sut = nil
        client.complete(withStatusCode: 200, data: makeItemJSON(item))
        
        XCTAssertTrue(capturedResults.isEmpty)
    }
    
    // MARK: - Helpers
    
    private func makeSUT(url: URL = URL(string: "https://any-url.com")!, file: StaticString = #file, line: UInt = #line) -> (sut: RemoteLoginRequester, client: HTTPClientSpy) {
        let client = HTTPClientSpy()
        let sut = RemoteLoginRequester(url: url, client: client)
        self.trackForMemoryLeaks(client, file: file, line: line)
        self.trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, client)
    }
    
    private func failure(_ error: RemoteLoginRequester.Error) -> RemoteLoginRequester.Result {
        return .failure(error)
    }
    
    private func expect(_ sut: RemoteLoginRequester, toCompleteWith expectedResult: RemoteLoginRequester.Result, when action: () -> Void, file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for request completion")
        
        sut.request(withData: Data()) { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedItem), .success(expectedItem)):
                XCTAssertEqual(receivedItem, expectedItem, file: file, line: line)
            case let (.failure(receivedError as RemoteLoginRequester.Error), .failure(expectedError as RemoteLoginRequester.Error)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)
            default:
                XCTFail("Expected result \(expectedResult) got \(receivedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
        action()
        
        wait(for: [exp], timeout: 1.0)
    }
    
    private func makeItemJSON(_ item: LoginHeaders) -> [String: String] {
        return [
            "access-token": item.accessToken,
            "client": item.client,
            "uid": item.uid
        ]
    }

}
