//
//  HTTPClientSpy.swift
//  LoginTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import Login

class HTTPClientSpy: HTTPClient {
    private var messages = [(url: URL, completion: (HTTPClient.Result) -> Void)]()
    
    var requestedURLs: [URL] {
        return self.messages.map { $0.url }
    }
    
    func post(url: URL, withData: Data, completion: @escaping Response) {
        self.messages.append((url, completion))
    }
    
    func complete(with error: Error, at index: Int = 0) {
        self.messages[index].completion(.failure(error))
    }
    
    func complete(withStatusCode code: Int, data: [String: String] = [:], at index: Int = 0) {
        let response = HTTPURLResponse(
            url: requestedURLs[index],
            statusCode: code,
            httpVersion: nil,
            headerFields: data
        )!
        messages[index].completion(.success((Data(), response)))
    }
    
}
