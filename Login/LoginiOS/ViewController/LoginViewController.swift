//
//  LoginViewController.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

import Login

final public class LoginViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var inputEmail: LoginInputView!
    @IBOutlet weak var inputPassword: LoginInputView!
    @IBOutlet weak var constHeaderSmallHeight: NSLayoutConstraint!
    
    // MARK: - IBActions
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
        self.requestLogin()
    }
    
    // MARK: - Properties
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private let viewModel: LoginViewModel
    private(set) public var progress: LoginProgressView?
    
    // MARK: - Life Cycle
    
    public init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: Bundle(for: LoginViewController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerKeyboardObservers()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeKeyboardObservers()
    }
    
}

private extension LoginViewController {
    
    // MARK: - Observers
    
    func registerKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification), name: UIApplication.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification), name: UIApplication.keyboardWillHideNotification, object: nil)
    }

    func removeKeyboardObservers() {
        [UIApplication.keyboardWillShowNotification, UIApplication.keyboardWillHideNotification].forEach {
            NotificationCenter.default.removeObserver(self, name: $0, object: nil)
        }
    }
    
    // MARK: - Setup
    
    func setupUI() {
        self.scrollView.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.lblTitle.font = .rubikFont(ofSize: 20)
        self.setupBtnLogin()
        self.setupInputEmail()
        self.setupInputPassword()
    }
    
    func setupBtnLogin() {
        self.btnLogin.layer.cornerRadius = 8
        self.btnLogin.titleLabel?.font = .rubikFont(ofSize: 16)
    }
    
    func setupInputEmail() {
        self.inputEmail.title = "Email"
        self.inputEmail.returnKeyType = .next
        self.inputEmail.keyboardType = .emailAddress
        self.inputEmail.onTextChange = { [weak self] text in
            self?.viewModel.email = text
        }
        self.inputEmail.onReturnPressed = { [weak self] in
            _ = self?.inputPassword.becomeFirstResponder()
        }
    }
    
    func setupInputPassword() {
        self.inputPassword.title = "Senha"
        self.inputPassword.returnKeyType = .go
        self.inputPassword.hasSecureContent = true
        self.inputPassword.onTextChange = { [weak self] text in
            self?.viewModel.password = text
        }
        self.inputPassword.onReturnPressed = { [weak self] in
            self?.requestLogin()
        }
    }
    
    // MARK: - Keyboard Presentation Handlers
    
    @objc func keyboardWillShowNotification(notification: Notification) {
        self.handleKeyboardPresentation(willPresent: true)
         if let userInfo = notification.userInfo {
             let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
             let keyboardHeight = keyboardRectangle?.height ?? 0
             self.scrollView.contentInset.bottom = keyboardHeight
         }
    }
    
    @objc func keyboardWillHideNotification() {
        self.handleKeyboardPresentation(willPresent: false)
        self.scrollView.contentInset.bottom = 0
    }
    
    func handleKeyboardPresentation(willPresent: Bool) {
        self.constHeaderSmallHeight.priority = .init(willPresent ? 999 : 250)
        if !willPresent {
            self.lblTitle.isHidden = false
        }
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
            self.lblTitle.alpha = willPresent ? 0 : 1
        }) { _ in
            if willPresent {
                self.lblTitle.isHidden = true
            }
        }
    }
    
    // MARK: - Progress
    
    func playProgress() {
        let progress = LoginProgressView(frame: self.view.bounds)
        self.view.addSubview(progress)
        self.progress = progress
    }
    
    func stopProgress() {
        self.progress?.removeFromSuperview()
        self.progress = nil
    }
    
    // MARK: - General Methods
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    func requestLogin() {
        [self.inputEmail, self.inputPassword].forEach { $0.removeErrorPresentation() }
        self.dismissKeyboard()
        self.playProgress()
        self.viewModel.login()
    }
    
}

// MARK: - Login View

extension LoginViewController: LoginView {
    
    public func didLoginSuccessfully() {
        self.stopProgress()
    }
    
    public func presentEmailError(_ message: String) {
        self.stopProgress()
        self.inputEmail.showError(message)
    }
    
    public func presentPasswordError(_ message: String) {
        self.stopProgress()
        self.inputPassword.showError(message)
    }
    
}

// MARK: - ScrollView Delegate

extension LoginViewController: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }
    
}
