//
//  LoginInputView.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

final class LoginInputView: UIView {
    
    // MARK: - UI Components

    private(set) public lazy var lblTitle: UILabel = self.createLblTitle()
    private(set) public lazy var viewTextContainer: UIView = self.createViewTextContainer()
    private(set) public lazy var textField: UITextField = self.createTextField()
    private(set) public lazy var btnSecureContent: UIButton = self.createBtnSecureContent()
    private(set) public lazy var btnClearText: UIButton = self.createBtnClearText()
    private(set) public lazy var lblError: UILabel = self.createLblError()
    
    // MARK: - Properties
    
    var title: String = "" {
        didSet {
            self.lblTitle.text = self.title
        }
    }
    var hasSecureContent = false {
        didSet {
            self.updateBtnSecureContent()
            self.btnSecureContent.isHidden = !self.hasSecureContent
        }
    }
    var text: String = "" {
        didSet {
            self.onTextChange?(self.text)
        }
    }
    var returnKeyType: UIReturnKeyType = .default {
        didSet {
            self.textField.returnKeyType = self.returnKeyType
        }
    }
    var keyboardType: UIKeyboardType = .asciiCapable {
        didSet {
            self.textField.keyboardType = self.keyboardType
        }
    }
    
    var onTextChange: ((String) -> Void)?
    var onReturnPressed: (() -> Void)?
    
    private var isContentSecured = true {
        didSet {
            self.updateBtnSecureContent()
        }
    }
    private let errorColor: UIColor = #colorLiteral(red: 0.8784313725, green: 0, blue: 0, alpha: 1)
    
    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupUI()
    }
    
    // MARK: - Responder Events
    
    override func becomeFirstResponder() -> Bool {
        self.textField.becomeFirstResponder()
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        self.textField.resignFirstResponder()
        return super.resignFirstResponder()
    }
    
    // MARK: - Error Presentation
    
    func showError(_ message: String) {
        self.lblError.text = message
        self.viewTextContainer.layer.borderColor = self.errorColor.cgColor
        self.textField.rightView = self.btnClearText
        self.textField.rightViewMode = .always
        self.lblError.isHidden = false
    }
    
    func removeErrorPresentation() {
        self.lblError.text = " "
        self.viewTextContainer.layer.borderColor = UIColor.clear.cgColor
        self.updateBtnSecureContent()
        self.lblError.isHidden = true
    }

}

private extension LoginInputView {
    
    // MARK: - Setup
    
    func setupUI() {
        self.setupLblTitle()
        self.setupViewTextContainer()
        self.setupTextField()
        self.setupLblError()
    }
    
    func setupLblTitle() {
        self.addSubview(self.lblTitle)
        
        self.addConstraints([
            self.lblTitle.topAnchor.constraint(equalTo: self.topAnchor),
            self.lblTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.lblTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    func setupViewTextContainer() {
        self.addSubview(self.viewTextContainer)
        
        self.addConstraints([
            self.viewTextContainer.topAnchor.constraint(equalTo: self.lblTitle.bottomAnchor, constant: 4),
            self.viewTextContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.viewTextContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    func setupTextField() {
        self.viewTextContainer.addSubview(self.textField)
        
        self.viewTextContainer.addConstraints([
            self.textField.topAnchor.constraint(equalTo: self.viewTextContainer.topAnchor),
            self.textField.centerXAnchor.constraint(equalTo: self.viewTextContainer.centerXAnchor),
            self.textField.widthAnchor.constraint(equalTo: self.viewTextContainer.widthAnchor, multiplier: 319/343),
            self.textField.bottomAnchor.constraint(equalTo: self.viewTextContainer.bottomAnchor)
        ])
    }
    
    func setupLblError() {
        self.addSubview(self.lblError)
        
        let errorHeightConstraint = self.lblError.heightAnchor.constraint(equalToConstant: 0)
        errorHeightConstraint.priority = .defaultLow
        self.addConstraints([
            self.lblError.topAnchor.constraint(equalTo: self.viewTextContainer.bottomAnchor, constant: 4),
            self.lblError.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 4),
            self.lblError.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.lblError.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -4),
            errorHeightConstraint
        ])
    }
    
    // MARK: - State Methods
    
    func updateBtnSecureContent() {
        self.textField.rightView = self.hasSecureContent ? self.btnSecureContent : nil
        self.textField.rightViewMode = self.hasSecureContent ? .always : .never
        self.textField.isSecureTextEntry = self.hasSecureContent ? self.isContentSecured : false
    }
    
    // MARK: - Objc Methods
    
    @objc func btnSecureContentAction() {
        self.isContentSecured.toggle()
    }
    
    @objc func btnClearTextAction() {
        self.textField.text = ""
        self.textFieldDidChange(self.textField)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.text = textField.text ?? ""
        if !self.lblError.isHidden {
            self.removeErrorPresentation()
        }
    }
    
    // MARK: - Factories
    
    func createLblTitle() -> UILabel {
        let label = UILabel()
        label.font = .rubikFont(ofSize: 14)
        label.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(.init(500), for: .vertical)
        label.setContentCompressionResistancePriority(.init(999), for: .vertical)
        return label
    }
    
    func createViewTextContainer() -> UIView {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.clear.cgColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    func createTextField() -> UITextField {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.font = .rubikFont(ofSize: 16)
        textField.textColor = .black
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.delegate = self
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    
    func createBtnSecureContent() -> UIButton {
        let button = UIButton(type: .system)
        let buttonImage = UIImage(named: "icon-eye", in: Bundle(for: LoginInputView.self), with: nil)
        button.setImage(buttonImage?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(self.btnSecureContentAction), for: .touchUpInside)
        return button
    }
    
    func createBtnClearText() -> UIButton {
        let button = UIButton(type: .system)
        let buttonImage = UIImage(named: "icon-error", in: Bundle(for: LoginInputView.self), with: nil)
        button.setImage(buttonImage?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(self.btnClearTextAction), for: .touchUpInside)
        return button
    }
    
    func createLblError() -> UILabel {
        let label = UILabel()
        label.font = .rubikFont(ofSize: 12)
        label.textColor = self.errorColor
        label.textAlignment = .right
        label.text = " "
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
}

// MARK: - TextField Delegate

extension LoginInputView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onReturnPressed?()
        return true
    }
    
}
