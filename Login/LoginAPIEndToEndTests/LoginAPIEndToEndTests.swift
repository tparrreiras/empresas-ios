//
//  LoginAPIEndToEndTests.swift
//  LoginAPIEndToEndTests
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import XCTest

@testable import Login

class LoginAPIEndToEndTests: XCTestCase {
 
    func test_endToEndTestServerGetHeadersResult_deliversSuccessOnValidCredentails() {
        let email = "testeapple@ioasys.com.br"
        
        switch getHeadersResult(forEmail: email) {
        case let .success(headers):
            XCTAssertEqual(headers.uid, email)
        case let .failure(error):
            XCTFail("Expected successful headers result, got \(error) instead")
        default:
            XCTFail("Expected successful headers result, got no result instead")
        }
    }
    
    // MARK: - Helpers
    
    private func getHeadersResult(forEmail email: String, file: StaticString = #file, line: UInt = #line) -> LoginRequester.Result? {
        let testServerURL = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in")!
        let client = URLSessionHTTPClient(session: URLSession.shared)
        let requester = RemoteLoginRequester(url: testServerURL, client: client)
        trackForMemoryLeaks(client, file: file, line: line)
        trackForMemoryLeaks(requester, file: file, line: line)
        let credentials = [
            "email": email,
            "password": "12341234"
        ]
        let data = try! JSONSerialization.data(withJSONObject: credentials)
        
        let exp = expectation(description: "Wait for request completion")
        
        var receivedResult: LoginRequester.Result?
        requester.request(withData: data) { result in
            receivedResult = result
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5.0)
        
        return receivedResult
    }

}
