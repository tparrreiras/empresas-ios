//
//  LoginRequester.swift
//  Login
//
//  Created by Tulio Parreiras on 08/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol LoginRequester {
    typealias Result = Swift.Result<LoginHeaders, Error>
    
    func request(withData data: Data, completion: @escaping(Result) -> Void)
}
