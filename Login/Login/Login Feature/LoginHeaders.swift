//
//  LoginHeaders.swift
//  Login
//
//  Created by Tulio Parreiras on 08/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public struct LoginHeaders: Equatable {
    public let accessToken: String
    public let client: String
    public let uid: String
    
    public init(
        accessToken: String,
        client: String,
        uid: String) {
        self.accessToken = accessToken
        self.client = client
        self.uid = uid
    }
}
