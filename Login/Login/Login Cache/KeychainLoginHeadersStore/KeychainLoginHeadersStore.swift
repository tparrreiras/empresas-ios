//
//  KeychainLoginHeadersStore.swift
//  Login
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class KeychainLoginHeadersStore: LoginHeadersStore {
    
    private let key: String
    
    public init(key: String) {
        self.key = key
    }
    
    public enum Error: Swift.Error {
        case invalidData
    }
    
    private struct Item: Encodable, Decodable {
        let accessToken: String
        let client: String
        let uid: String
        
        var item: LoginHeaders {
            return LoginHeaders(accessToken: accessToken, client: client, uid: uid)
        }
        
        static func createItem(from loginHeaders: LoginHeaders) -> Item {
            return Item(accessToken: loginHeaders.accessToken, client: loginHeaders.client, uid: loginHeaders.uid)
        }
    }
    
    public func insert(_ loginHeaders: LoginHeaders, completion: @escaping InsertionCompletion) {
        guard let data = try? JSONEncoder().encode(Item.createItem(from: loginHeaders)) else {
            completion(.failure(Error.invalidData))
            return
        }
        KeychainStore.save(data: data, forKey: self.key) { [weak self] result in
            guard self != nil else { return }
            completion(result)
        }
    }
    
    public func load(completion: @escaping LoadCompletion) {
        KeychainStore.loadData(forKey: self.key) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case let .success(data):
                completion(Result {
                    try JSONDecoder().decode(Item.self, from: data).item
                })
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    public func deleteCachedLoginHeaders(completion: @escaping DeletionCompletion) {
        KeychainStore.deleteCache(forKey: self.key) { [weak self] result in
            guard self != nil else { return }
            completion(result)
        }
    }
    
}
