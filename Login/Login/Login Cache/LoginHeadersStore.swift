//
//  LoginHeadersStore.swift
//  Login
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol LoginHeadersStore {
    typealias InsertionResult = Swift.Result<Void, Error>
    typealias InsertionCompletion = (InsertionResult) -> Void
    
    typealias LoadResult = Swift.Result<LoginHeaders, Error>
    typealias LoadCompletion = (LoadResult) -> Void
    
    typealias DeletionResult = Swift.Result<Void, Error>
    typealias DeletionCompletion = (DeletionResult) -> Void
    
    func insert(_ loginHeaders: LoginHeaders, completion: @escaping InsertionCompletion)
    func load(completion: @escaping LoadCompletion)
    func deleteCachedLoginHeaders(completion: @escaping DeletionCompletion)
    
}

