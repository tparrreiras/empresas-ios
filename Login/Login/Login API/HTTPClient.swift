//
//  HTTPClient.swift
//  Login
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol HTTPClient {
    typealias Result = Swift.Result<(Data, HTTPURLResponse), Error>
    typealias Response = (Result) -> Void
    
    func post(url: URL, withData data: Data, completion: @escaping Response)
}
