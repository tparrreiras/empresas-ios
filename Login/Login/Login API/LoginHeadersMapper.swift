//
//  LoginHeadersMapper.swift
//  Login
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

class LoginHeadersMapper {
    
    private struct Item: Decodable {
        let accessToken: String
        let client: String
        let uid: String
        
        var item: LoginHeaders {
            return LoginHeaders(accessToken: accessToken, client: client, uid: uid)
        }
        
        private enum CodingKeys: String, CodingKey {
            case client, uid
            case accessToken = "access-token"
        }
        
    }
    
    static var OK_200: Int { return 200}
    
    internal static func map(_ data: Data, _ response: HTTPURLResponse) throws -> LoginHeaders {
        guard response.statusCode == OK_200, let headers = try? JSONSerialization.data(withJSONObject: response.allHeaderFields), let item = try? JSONDecoder().decode(Item.self, from: headers) else {
            throw RemoteLoginRequester.Error.invalidData
        }
        return item.item
    }
}

