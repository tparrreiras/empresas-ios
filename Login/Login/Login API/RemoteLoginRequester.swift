//
//  RemoteLoginRequester.swift
//  Login
//
//  Created by Tulio Parreiras on 09/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class RemoteLoginRequester: LoginRequester {
    
    private let url: URL
    private let client: HTTPClient
    
    public typealias Result = LoginRequester.Result
    
    public enum Error: Swift.Error {
        case connectivity
        case invalidData
    }
    
    public init(url: URL, client: HTTPClient = URLSessionHTTPClient(session: .shared)) {
        self.url = url
        self.client = client
    }
    
    public func request(withData data: Data, completion: @escaping (Result) -> Void) {
        self.client.post(url: self.url, withData: data) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case let .success((data, response)):
                completion(Result {
                    try LoginHeadersMapper.map(data, response)
                })
            case .failure:
                completion(.failure(Error.connectivity))
            }
        }
    }
}
