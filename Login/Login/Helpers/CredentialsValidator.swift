//
//  CredentialsValidator.swift
//  Login
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class CredentialsValidator {
    
    private init() { }
    
    public static func validateEmail(_ email: String) -> Bool {
        return !email.isEmpty
    }
    
    public static func validatePassword(_ password: String) -> Bool {
        return !password.isEmpty
    }
}
