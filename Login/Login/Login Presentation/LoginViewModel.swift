//
//  LoginViewModel.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class LoginViewModel {
    
    // MARK: - Properties
    
    public weak var loginView: LoginView?

    public var onLoginSuccess: ((LoginHeaders) -> Void)?
    
    public var email: String = ""
    public var password: String = ""
    
    private let loginRequester: LoginRequester
    
    // MARK: - Life Cycle
    
    public init(loginRequester: LoginRequester) {
        self.loginRequester = loginRequester
    }
    
    // MARK: - Login
    
    public func login() {
        guard self.validateCredentials() else { return }
        let bodyJSON = [
            "email": email,
            "password": password
        ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON)
        self.loginRequester.request(withData: data) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(headers):
                self.loginView?.didLoginSuccessfully()
                self.onLoginSuccess?(headers)
            case .failure:
                self.loginView?.presentEmailError(" ")
                self.loginView?.presentPasswordError("Credenciais incorretas")
            }
        }
    }
    
    // MARK: - Validation
    
    private func validateCredentials() -> Bool {
        guard CredentialsValidator.validateEmail(self.email) else {
            self.loginView?.presentEmailError("Informe um email válido")
            return false
        }
        guard CredentialsValidator.validatePassword(self.password) else {
            self.loginView?.presentPasswordError("Informe uma senha válida")
            return false
        }
        return true
    }
    
}
