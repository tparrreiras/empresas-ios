//
//  LoginView.swift
//  Login
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol LoginView: class {
    func didLoginSuccessfully()
    func presentEmailError(_ message: String)
    func presentPasswordError(_ message: String)
}
