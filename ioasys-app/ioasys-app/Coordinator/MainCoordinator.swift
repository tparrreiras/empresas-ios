//
//  MainCoordinator.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

import CompanySearch
import CompanySearchiOS

import Login
import LoginiOS

final class MainCoordinator {
    
    private let window: UIWindow
    private let loginHeadersStore: LoginHeadersStore
    private var rootController: UIViewController? {
        return self.window.rootViewController
    }
    
    init(window: UIWindow, loginHeadersStore: LoginHeadersStore = KeychainLoginHeadersStore(key: String(describing: LoginHeaders.self))) {
        self.window = window
        self.loginHeadersStore = MainQueueDispatchDecorator(decoratee: loginHeadersStore)
    }
    
    func start() {
        self.lookForCachedSession()
    }
    
    private func lookForCachedSession() {
        self.setFakeLaunch()
        self.loginHeadersStore.load { [weak self] result in
            switch result {
            case let .success(headers):
                self?.startHomeFlow(with: headers)
            case .failure:
                self?.startAuthFlow()
            }
        }
    }
    
    private func setFakeLaunch() {
        let controller = FakeLaunchViewController(nibName: nil, bundle: nil)
        self.window.rootViewController = controller
        self.window.makeKeyAndVisible()
    }
    
    private func startAuthFlow() {
        let controller = self.getAuthController()
        self.window.switchRootViewController(controller, animated: true, transition: .transitionCrossDissolve, completion: nil)
    }
    
    private func startHomeFlow(with headers: LoginHeaders) {
        let controller = self.getHomeController(with: headers)
        self.window.switchRootViewController(controller, animated: true, transition: .transitionCrossDissolve, completion: nil)
    }
    
    private func presentHome(with headers: LoginHeaders) {
        let controller = self.getHomeController(with: headers)
        self.window.switchRootViewController(controller, animated: true, completion: nil)
    }
    
    private func presentDetails(forCompany company: Company) {
        let imageLoader = RemoteImageDataLoader()
        let controller = CompanyDetailsUIComposer.companyDetailsComposedWith(company: company, imageLoader: imageLoader)
        (self.rootController as? UINavigationController)?.pushViewController(controller, animated: true)
    }
    
    private func presentAuth(completion: (() -> Void)? = nil) {
        self.loginHeadersStore.deleteCachedLoginHeaders(completion: { _ in })
        let controller = self.getAuthController()
        self.window.switchRootViewController(controller, animated: true, completion: completion)
    }
    
    private func presentSessionExpiredAlert() {
        let alert = UIAlertController(title: "Sessão expirada", message: "Realize o login novamente", preferredStyle: .alert)
        self.window.rootViewController?.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            alert.dismiss(animated: true, completion: nil)
        }
    }

    // MARK: - Factory
    
    private func getAuthController() -> UIViewController {
        let url = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in")!
        let loginRequester = RemoteLoginRequester(url: url, client: URLSessionHTTPClient(session: .shared))
        let loginRequesterDecorator = LoginRequesterCacheDecorator(decoratee: loginRequester, cache: self.loginHeadersStore)
        
        let controller = LoginUIComposer.loginComposedWith(loginRequester: loginRequesterDecorator, onLoginSuccess: { [weak self] headers in
            self?.presentHome(with: headers)
        })
        return controller
    }
    
    private func getHomeController(with headers: LoginHeaders) -> UIViewController {
        let url = URL(string: "https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=2&name=")!
        let companySearcher = CompanySearcherSessionHandleDecorator(decoratee: RemoteCompanySearcher(url: url), invalidSessionHandler: {
            self.presentAuth { [weak self] in
                self?.presentSessionExpiredAlert()
            }
        })
        let headers = SearchHeaders(accessToken: headers.accessToken, client: headers.client, uid: headers.uid)
        let imageLoader = RemoteImageDataLoader()
        let companySearchController = CompanySearchUIComposer.companySearchComposedWith(companySearcher: companySearcher, headers: headers, imageLoader: imageLoader, onCompanySelection: { [weak self] company in
            self?.presentDetails(forCompany: company)
        })
        companySearchController.onLogoutAction = { [weak self] in
            self?.presentAuth()
        }
        let navigationController = NavigationController(rootViewController: companySearchController)
        navigationController.setTransparentBar()
        return navigationController
    }
    
}

