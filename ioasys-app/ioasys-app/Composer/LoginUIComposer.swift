//
//  LoginUIComposer.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import Login
import LoginiOS

final public class LoginUIComposer {
    
    private init() { }
    
    public static func loginComposedWith(loginRequester: LoginRequester, onLoginSuccess: @escaping (LoginHeaders) -> Void) -> LoginViewController {
        let viewModel = LoginViewModel(loginRequester: MainQueueDispatchDecorator(decoratee:loginRequester))
        let loginController = LoginViewController(viewModel: viewModel)
        viewModel.loginView = loginController
        viewModel.onLoginSuccess = onLoginSuccess
        return loginController
    }
    
}
