//
//  CompanyDetailsUIComposer.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import CompanySearch
import CompanySearchiOS

final public class CompanyDetailsUIComposer {
    
    private init() { }
    
    static func companyDetailsComposedWith(company: Company, imageLoader: ImageDataLoader) -> CompanyDetailsViewController {
        let viewModel = CompanyDetailsViewModel(company: company, imageLoader: MainQueueDispatchDecorator(decoratee: imageLoader))
        let controller = CompanyDetailsViewController(viewModel: viewModel)
        viewModel.companyDetailsView = controller
        controller.title = company.name.capitalized
        return controller
    }
    
}
