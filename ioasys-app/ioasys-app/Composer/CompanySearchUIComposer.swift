//
//  CompanySearchUIComposer.swift
//  CompanySearchiOS
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import CompanySearch
import CompanySearchiOS

final public class CompanySearchUIComposer {
    
    private init() { }
    
    public static func companySearchComposedWith(companySearcher: CompanySearcher, headers: SearchHeaders, imageLoader: ImageDataLoader, onCompanySelection: @escaping (Company) -> Void) -> CompanySearchViewController {
        let viewModel = CompanySearchViewModel(companySearcher: MainQueueDispatchDecorator(decoratee: companySearcher), headers: headers, imageLoader: MainQueueDispatchDecorator(decoratee: imageLoader))
        let controller = CompanySearchViewController(viewModel: viewModel, onCompanySelection: onCompanySelection)
        viewModel.companyView = controller
        return controller
    }
}
