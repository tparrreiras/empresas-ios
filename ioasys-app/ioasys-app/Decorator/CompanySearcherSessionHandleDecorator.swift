//
//  CompanySearcherSessionHandleDecorator.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import CompanySearch

final class CompanySearcherSessionHandleDecorator: CompanySearcher {
    
    private let decoratee: CompanySearcher
    private var invalidSessionHandler: (() -> Void)
    
    init(decoratee: CompanySearcher, invalidSessionHandler: @escaping () -> Void) {
        self.decoratee = MainQueueDispatchDecorator(decoratee: decoratee)
        self.invalidSessionHandler = invalidSessionHandler
    }
    
    func searchCompanies(for searchText: String, with headers: [String : String], completion: @escaping (SearchResult) -> Void) -> HTTPClientTask {
        decoratee.searchCompanies(for: searchText, with: headers) { [weak self] result in
            completion(result.mapError({
                if let error = $0 as? CustomError, error.isInvalidSessionError {
                    self?.invalidSessionHandler()
                }
                return $0
            }))
        }
    }
    
}

private extension CustomError {
    var isInvalidSessionError: Bool {
        return !self.errors.filter({ $0.lowercased().contains("you need to sign in") }).isEmpty
    }
}
