//
//  MainQueueDispatchDecorator.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import CompanySearch

import Login

final class MainQueueDispatchDecorator<T> {
    private let decoratee: T
    
    init(decoratee: T) {
        self.decoratee = decoratee
    }
    
    func dispatch(completion: @escaping () -> Void) {
        guard Thread.isMainThread else {
            return DispatchQueue.main.async(execute: completion)
        }
        
        completion()
    }
}

extension MainQueueDispatchDecorator: CompanySearcher where T == CompanySearcher {
    
    func searchCompanies(for searchText: String, with headers: [String : String], completion: @escaping (SearchResult) -> Void) -> HTTPClientTask {
        decoratee.searchCompanies(for: searchText, with: headers) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
}

extension MainQueueDispatchDecorator: ImageDataLoader where T == ImageDataLoader {
    
    func loadImageData(from url: URL, completion: @escaping (ImageDataLoader.Result) -> Void) -> ImageDataLoaderTask {
        decoratee.loadImageData(from: url, completion: { [weak self] result in
            self?.dispatch { completion(result) }
        })
    }
    
}

extension MainQueueDispatchDecorator: LoginRequester where T == LoginRequester {
    
    func request(withData data: Data, completion: @escaping (LoginRequester.Result) -> Void) {
        decoratee.request(withData: data) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
}

extension MainQueueDispatchDecorator: LoginHeadersStore where T == LoginHeadersStore {
    
    func load(completion: @escaping LoadCompletion) {
        decoratee.load { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
    func insert(_ loginHeaders: LoginHeaders, completion: @escaping InsertionCompletion) {
        decoratee.insert(loginHeaders) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
    func deleteCachedLoginHeaders(completion: @escaping DeletionCompletion) {
        decoratee.deleteCachedLoginHeaders { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
}
