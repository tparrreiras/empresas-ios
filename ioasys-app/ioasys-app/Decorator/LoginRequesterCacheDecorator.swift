//
//  LoginRequesterCacheDecorator.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import Login

final class LoginRequesterCacheDecorator: LoginRequester {
    
    private let decoratee: LoginRequester
    private let cache: LoginHeadersStore
    
    init(decoratee: LoginRequester, cache: LoginHeadersStore) {
        self.decoratee = decoratee
        self.cache = cache
    }
    
    func request(withData data: Data, completion: @escaping (LoginRequester.Result) -> Void) {
        decoratee.request(withData: data) { [weak self] result in
            completion(result.map { loginHeaders in
                self?.cache.insert(loginHeaders, completion: { _ in })
                return loginHeaders
            })
        }
    }
    
}
