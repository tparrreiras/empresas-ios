//
//  UIWindow+SwitchRoot.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

public extension UIWindow {
    
    func switchRootViewController(_ rootViewController: UIViewController, animated: Bool, transition options: UIView.AnimationOptions = .transitionFlipFromRight, completion: (() -> Void)?) {
        if animated {
            UIView.transition(with: self, duration: 0.5, options: options, animations: {
                let oldState = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.rootViewController = rootViewController
                UIView.setAnimationsEnabled(oldState)
            }, completion: { finished in
                if finished {
                    completion?()
                }
            })
        }
        else {
            self.rootViewController = rootViewController
            completion?()
        }
    }

}
