//
//  NavigationController.swift
//  ioasys-app
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.children.last?.preferredStatusBarStyle ?? .lightContent
    }
    
}

extension UINavigationController {
    
    func setTransparentBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
    }
    
}
