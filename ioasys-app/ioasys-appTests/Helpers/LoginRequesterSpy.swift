//
//  LoginRequesterSpy.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import Login

final class LoginRequesterSpy: LoginRequester {
    
    private var loginRequests = [(LoginRequester.Result) -> Void]()
    
    var loginRequestCallCount: Int {
        return self.loginRequests.count
    }
    
    func request(withData data: Data, completion: @escaping (LoginRequester.Result) -> Void) {
        loginRequests.append(completion)
    }
    
    func completeLoginRequest(with headers: LoginHeaders = LoginHeaders(accessToken: "", client: "", uid: ""), at index: Int = 0) {
        loginRequests[index](.success(headers))
    }
    
    func completeLoginRequestWithError(at index: Int = 0) {
        let error = NSError(domain: "an error", code: 0)
        loginRequests[index](.failure(error))
    }
    
}
