//
//  LoaderSpy.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

import CompanySearch

final class LoaderSpy: CompanySearcher, ImageDataLoader {
    
    // MARK: - CompanySearcher
    
    private var searchRequests = [(CompanySearcher.SearchResult) -> Void]()
    
    var searchRequestsCount: Int {
        return self.searchRequests.count
    }
    
    private struct MockTask: HTTPClientTask {
        func cancel() { }
    }
    
    func searchCompanies(for searchText: String, with headers: [String : String], completion: @escaping (CompanySearcher.SearchResult) -> Void) -> HTTPClientTask {
        searchRequests.append(completion)
        return MockTask()
    }
    
    func completeSearch(with companies: [Company] = [], at index: Int = 0) {
        searchRequests[index](.success(companies))
    }
    
    func completeSearchWithError(at index: Int = 0) {
        searchRequests[index](.failure(NSError(domain: "error", code: 0)))
    }
    
    // MARK: - ImageDataLoader
    
    private var imageRequests = [(url: URL, completion: (ImageDataLoader.Result) -> Void)]()
    
    var loadedImageURLs: [URL] {
        return self.imageRequests.map { $0.url }
    }
    
    private(set) var cancelledImageURLs = [URL]()
    
    private struct ImageTaskSpy: ImageDataLoaderTask {
        let cancelCallback: () -> Void
        func cancel() { cancelCallback() }
    }
    
    func loadImageData(from url: URL, completion: @escaping (ImageDataLoader.Result) -> Void) -> ImageDataLoaderTask {
        imageRequests.append((url, completion))
        return ImageTaskSpy { [weak self] in self?.cancelledImageURLs.append(url) }
    }
    
    func completeImageLoading(with imageData: Data = Data(), at index: Int = 0) {
        imageRequests[index].completion(.success(imageData))
    }
    
}
