//
//  LoginViewController+TestHelpers.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

@testable import LoginiOS

extension LoginViewController {
    
    var isShowingLoadingIndicator: Bool {
        return self.progress != nil && self.progress?.superview == self.view
    }
    
    var isShowingEmailError: Bool {
        return self.inputEmail.isShowingEmailError
    }
    
    var isShowingPasswordError: Bool {
        return self.inputPassword.isShowingEmailError
    }
    
    func fillEmailField(withText text: String) {
        self.inputEmail.fillText(text)
    }
    
    func fillPasswordField(withText text: String) {
        self.inputPassword.fillText(text)
    }
    
    func simulateLoginButtonTap() {
        self.btnLogin.sendActions(for: .touchUpInside)
    }
    
}
