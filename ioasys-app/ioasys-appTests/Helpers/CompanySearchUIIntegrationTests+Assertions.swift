//
//  CompanySearchUIIntegrationTests+Assertions.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import XCTest

import CompanySearch
import CompanySearchiOS

extension CompanySearchUIIntegrationTests {

    func assertThat(_ sut: CompanySearchViewController, isRendering companies: [Company], file: StaticString = #file, line: UInt = #line) {
        sut.view.enforceLayoutCycle()
        
        guard sut.numberOfRenderedCompanyCells() == companies.count else {
            return XCTFail("Expected \(companies.count) cells, got \(sut.numberOfRenderedCompanyCells()) instead.", file: file, line: line)
        }
        
        companies.enumerated().forEach { index, image in
            assertThat(sut, hasViewConfiguredFor: image, at: index, file: file, line: line)
        }
    }
    
    func assertThat(_ sut: CompanySearchViewController, hasViewConfiguredFor company: Company, at index: Int, file: StaticString = #file, line: UInt = #line) {
        let view = sut.companyCell(at: index)
        
        guard let cell = view as? CompanyCell else {
            return XCTFail("Expected \(CompanyCell.self) instance, got \(String(describing: view)) instead", file: file, line: line)
        }
        
        XCTAssertEqual(cell.nameText?.lowercased(), company.name.lowercased(), "Expected name text to be \(String(describing: company.name)) for company cell at index (\(index))", file: file, line: line)
    }
    
}
