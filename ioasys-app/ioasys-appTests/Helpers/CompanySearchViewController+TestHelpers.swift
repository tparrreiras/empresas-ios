//
//  CompanySearchViewController+TestHelpers.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit
@testable import CompanySearchiOS

extension CompanySearchViewController {
    
    var isShowingSearchingIndicator: Bool {
        return self.progress != nil && self.progress?.superview == self.tableView
    }
    
    var isShowingEmptyResultError: Bool {
        return !self.lblError.isHidden
    }
    
    var companyCellsSection: Int { return 0 }
    
    func simulateSearch(withText text: String = "") {
        self.txfSearch.text = text
        self.txfSearch.sendActions(for: .editingChanged)
    }
    
    func numberOfRenderedCompanyCells() -> Int {
        return tableView.numberOfRows(inSection: companyCellsSection)
    }
    
    func companyCell(at row: Int) -> UITableViewCell? {
        guard numberOfRenderedCompanyCells() > row else {
            return nil
        }
        let ds = tableView.dataSource
        let index = IndexPath(row: row, section: companyCellsSection)
        return ds?.tableView(tableView, cellForRowAt: index)
    }
    
    @discardableResult
    func simulateCompanyCellVisible(at index: Int) -> CompanyCell? {
        return companyCell(at: index) as? CompanyCell
    }
    
    @discardableResult
    func simulateCompanyCellNotVisible(at row: Int) -> CompanyCell? {
        let view = simulateCompanyCellVisible(at: row)
        
        let delegate = tableView.delegate
        let index = IndexPath(row: row, section: companyCellsSection)
        delegate?.tableView?(tableView, didEndDisplaying: view!, forRowAt: index)
        
        return view
    }
    
}
