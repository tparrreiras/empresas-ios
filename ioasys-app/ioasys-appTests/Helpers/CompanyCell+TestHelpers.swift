//
//  CompanyCell+TestHelpers.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation
import CompanySearchiOS

extension CompanyCell {
    var nameText: String? {
        return self.lblTitle.text
    }
    
    var renderedImage: Data? {
        return self.imvBackground.image?.pngData()
    }
}
