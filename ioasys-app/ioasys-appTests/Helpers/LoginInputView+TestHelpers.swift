//
//  LoginInputView+TestHelpers.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

@testable import LoginiOS


extension LoginInputView {
    
    var isShowingEmailError: Bool {
        return !(self.lblError.text ?? "").isEmpty && !self.lblError.isHidden
    }
    
    func fillText(_ text: String) {
        self.textField.text = text
        self.textField.sendActions(for: .editingChanged)
    }
    
}
