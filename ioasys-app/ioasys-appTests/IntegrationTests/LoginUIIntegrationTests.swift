//
//  LoginUIIntegrationTests.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import XCTest

import ioasys_app
@testable import Login
@testable import LoginiOS

class LoginUIIntegrationTests: XCTestCase {

    func test_loginActions_doesNotRequestLoginWithInvalidCredentials() {
        let (sut, loginRequester) = makeSUT()
        sut.loadViewIfNeeded()
        
        sut.fillEmailField(withText: "")
        sut.fillPasswordField(withText: "")
        
        sut.simulateLoginButtonTap()
        
        XCTAssertEqual(loginRequester.loginRequestCallCount, 0)
    }
    
    func test_loginActions_doesNotDisplayErrorAfterTextEdit() {
        let (sut, _) = makeSUT()
        sut.loadViewIfNeeded()
        
        sut.fillEmailField(withText: "")
        sut.fillPasswordField(withText: "")
        XCTAssertFalse(sut.isShowingEmailError)
        
        sut.simulateLoginButtonTap()
        XCTAssertTrue(sut.isShowingEmailError)
        
        sut.fillEmailField(withText: "test@email.clom")
        sut.simulateLoginButtonTap()
        XCTAssertFalse(sut.isShowingEmailError)
        XCTAssertTrue(sut.isShowingPasswordError)
        
        sut.fillPasswordField(withText: "123456")
        
        sut.simulateLoginButtonTap()
        XCTAssertFalse(sut.isShowingPasswordError)
    }
    
    func test_loginActions_displayErrorsOnLoginFailure() {
        let (sut, loginRequester) = makeSUT()
        sut.loadViewIfNeeded()
        
        sut.fillEmailField(withText: "test@email.clom")
        sut.fillPasswordField(withText: "123456")
        
        sut.simulateLoginButtonTap()
        
        loginRequester.completeLoginRequestWithError()
        
        XCTAssertTrue(sut.isShowingEmailError)
        XCTAssertTrue(sut.isShowingPasswordError)
    }
    
    func test_loadIndicator_isVisibleWhenRequestingLogin() {
        let (sut, loginRequester) = makeSUT()
        sut.loadViewIfNeeded()
        
        sut.fillEmailField(withText: "test@email.clom")
        sut.fillPasswordField(withText: "123456")
        
        XCTAssertFalse(sut.isShowingLoadingIndicator)
        
        sut.simulateLoginButtonTap()
        XCTAssertTrue(sut.isShowingLoadingIndicator)
        
        loginRequester.completeLoginRequestWithError()
        XCTAssertFalse(sut.isShowingLoadingIndicator)
        
        sut.simulateLoginButtonTap()
        XCTAssertTrue(sut.isShowingLoadingIndicator)
        
        loginRequester.completeLoginRequest()
        XCTAssertFalse(sut.isShowingLoadingIndicator)
    }
    
    // MARK: - Helper
    
    private func makeSUT(onLoginSuccess: @escaping ((LoginHeaders) -> Void) = { _ in }) -> (sut: LoginViewController, loginRequester: LoginRequesterSpy) {
        let requesterSpy = LoginRequesterSpy()
        let sut = LoginUIComposer.loginComposedWith(loginRequester: requesterSpy, onLoginSuccess: onLoginSuccess)
        trackForMemoryLeaks(requesterSpy)
        trackForMemoryLeaks(sut)
        return (sut, requesterSpy)
    }

}
