//
//  CompanySearchUIIntegrationTests.swift
//  ioasys-appTests
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import XCTest

import ioasys_app
import CompanySearch
import CompanySearchiOS

class CompanySearchUIIntegrationTests: XCTestCase {

    func test_searchActions_requestSearchOnTextChange() {
        let (sut, spy) = makeSUT()
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(spy.searchRequestsCount, 0)
        
        sut.simulateSearch(withText: "a")
        XCTAssertEqual(spy.searchRequestsCount, 1)
        
        sut.simulateSearch(withText: "ab")
        XCTAssertEqual(spy.searchRequestsCount, 2)
    }
    
    func test_searchActions_displayErrorForEmptyResult() {
        let (sut, spy) = makeSUT()
        sut.loadViewIfNeeded()
        XCTAssertFalse(sut.isShowingEmptyResultError)
        
        sut.simulateSearch()
        XCTAssertFalse(sut.isShowingEmptyResultError)
        
        spy.completeSearch()
        XCTAssertTrue(sut.isShowingEmptyResultError)
        
        sut.simulateSearch()
        XCTAssertFalse(sut.isShowingEmptyResultError)
        
        spy.completeSearch(with: anyCompanies(), at: 1)
        XCTAssertFalse(sut.isShowingEmptyResultError)
    }
    
    func test_searchCompletion_rendersSuccessfullyLoadedCompanies() {
        let company0 = Company(description: "a description", id: 1, name: "a name", photo: "photo/path")
        let company1 = Company(description: "another description", id: 2, name: "another name", photo: "another/photo/path")
        let company2 = Company(description: "other description", id: 3, name: "other name", photo: "other/photo/path")
        let (sut, spy) = makeSUT()
        
        sut.loadViewIfNeeded()
        assertThat(sut, isRendering: [])
        
        sut.simulateSearch()
        spy.completeSearch(with: [company0])
        assertThat(sut, isRendering: [company0])
        
        sut.simulateSearch()
        spy.completeSearch(with: [company0, company1, company2], at: 1)
        assertThat(sut, isRendering: [company0, company1, company2])
        
        sut.simulateSearch()
        spy.completeSearch(with: [], at: 2)
        assertThat(sut, isRendering: [])
    }
    
    func test_searchingCompaniesIndicator_isVisibleWhileSearchingCompanies() {
        let (sut, spy) = makeSUT()
        sut.loadViewIfNeeded()
        XCTAssertFalse(sut.isShowingSearchingIndicator)
        
        sut.simulateSearch()
        XCTAssertTrue(sut.isShowingSearchingIndicator)
        
        spy.completeSearch()
        XCTAssertFalse(sut.isShowingSearchingIndicator)
        
        sut.simulateSearch()
        XCTAssertTrue(sut.isShowingSearchingIndicator)
        
        spy.completeSearchWithError(at: 1)
        XCTAssertFalse(sut.isShowingSearchingIndicator)
    }
    
    func test_companyCell_loadsImageURLWhenVisible() {
        let company0 = Company(description: "a description", id: 1, name: "a name", photo: "photo/path")
        let company1 = Company(description: "another description", id: 2, name: "another name", photo: "another/photo/path")
        let (sut, spy) = makeSUT()
        
        sut.loadViewIfNeeded()
        sut.simulateSearch()
        spy.completeSearch(with: [company0, company1])
        
        XCTAssertEqual(spy.loadedImageURLs, [], "Expected no image URL requests until views become visible")

        sut.simulateCompanyCellVisible(at: 0)
        XCTAssertEqual(spy.loadedImageURLs.count, 1)
        XCTAssertTrue(spy.loadedImageURLs.first?.absoluteString.contains(company0.photo) == true, "Expected first image URL request once first view becomes visible")

        sut.simulateCompanyCellVisible(at: 1)
        let companies = [company0, company1]
        XCTAssertEqual(spy.loadedImageURLs.count, companies.count)
        spy.loadedImageURLs.enumerated().forEach { index, url in
            XCTAssertTrue(url.absoluteString.contains(companies[index].photo), "Expected image at \(index) position URL request once view at \(index) position becomes visible")
        }
    }
    
    func test_companyCell_cancelsImageLoadingWHenNotVisibleAnymore() {
        let company0 = Company(description: "a description", id: 1, name: "a name", photo: "photo/path")
        let company1 = Company(description: "another description", id: 2, name: "another name", photo: "another/photo/path")
        let (sut, spy) = makeSUT()
        
        sut.loadViewIfNeeded()
        sut.simulateSearch()
        spy.completeSearch(with: [company0, company1])
        XCTAssertEqual(spy.cancelledImageURLs, [], "Expected no cancelled image URL requests until image is not visible")
        
        sut.simulateCompanyCellNotVisible(at: 0)
        XCTAssertTrue(spy.cancelledImageURLs.first?.absoluteString.contains(company0.photo) == true, "Expected one cancelled image URL request once first image is not visible anymore")
        
        sut.simulateCompanyCellNotVisible(at: 1)
        let companies = [company0, company1]
        XCTAssertEqual(spy.cancelledImageURLs.count, companies.count)
        spy.cancelledImageURLs.enumerated().forEach { index, url in
            XCTAssertTrue(url.absoluteString.contains(companies[index].photo), "Expected image at \(index) position URL request cancelled once view at \(index) position is not visible anymore")
        }
        
    }
    
    func test_companyCell_rendersImageLoadedFromURL() {
        let (sut, spy) = makeSUT()
        
        sut.loadViewIfNeeded()
        sut.simulateSearch()
        spy.completeSearch(with: anyCompanies())
        
        var view0 = sut.simulateCompanyCellVisible(at: 0)
        var view1 = sut.simulateCompanyCellVisible(at: 1)
        var view2 = sut.simulateCompanyCellVisible(at: 2)
        XCTAssertEqual(view0?.renderedImage, .none, "Expected no image for first view while loading first image")
        XCTAssertEqual(view1?.renderedImage, .none, "Expected no image for second view while loading second image")
        XCTAssertEqual(view2?.renderedImage, .none, "Expected no image for third view while loading third image")
        
        let imageData0 = UIImage.make(withColor: .red).pngData()!
        spy.completeImageLoading(with: imageData0)
        view0 = sut.simulateCompanyCellVisible(at: 0)
        XCTAssertEqual(view0?.renderedImage, imageData0, "Expected image for first view once first image loading completes successfully")
        XCTAssertEqual(view1?.renderedImage, .none, "Expected no image state change for second view once first image loading completes successfully")
        XCTAssertEqual(view2?.renderedImage, .none, "Expected no image state change for third view once first image loading completes successfully")
        
        let imageData1 = UIImage.make(withColor: .blue).pngData()!
        spy.completeImageLoading(with: imageData1, at: 1)
        view1 = sut.simulateCompanyCellVisible(at: 1)
        XCTAssertEqual(view0?.renderedImage, imageData0, "Expected no image state change for first view once second image loading completes successfully")
        XCTAssertEqual(view1?.renderedImage, imageData1, "Expected image for second view once second image loading completes successfully")
        XCTAssertEqual(view2?.renderedImage, .none, "Expected no image state change for third view once second image loading completes successfully")
        
        let imageData2 = UIImage.make(withColor: .green).pngData()!
        spy.completeImageLoading(with: imageData2, at: 2)
        view2 = sut.simulateCompanyCellVisible(at: 2)
        XCTAssertEqual(view0?.renderedImage, imageData0, "Expected no image state change for first view once third image loading completes successfully")
        XCTAssertEqual(view1?.renderedImage, imageData1, "Expected no image state change for second view once third image loading completes successfully")
        XCTAssertEqual(view2?.renderedImage, imageData2, "Expected image for third view once third image loading completes successfully")
    }
    
    func test_companyCell_returnsCompanyAtDidSelectEvent() {
        var selectedCompany: Company?
        let exp = expectation(description: "Wait for selection")
        
        let (sut, spy) = makeSUT(onCompanySelection: { [weak self] company in
            guard self != nil else { return }
            selectedCompany = company
            exp.fulfill()
        })
        
        let companies = anyCompanies()
        sut.loadViewIfNeeded()
        sut.simulateSearch()
        spy.completeSearch(with: companies)
        
        sut.tableView.delegate?.tableView?(sut.tableView, didSelectRowAt: IndexPath(row: 0, section: 1))
        
        wait(for: [exp], timeout: 1.0)
        
        XCTAssertNotNil(selectedCompany, "Expected to receive \(companies.first!), got no value instead")
        XCTAssertEqual(companies.first, selectedCompany, "Expected to receive \(companies.first!), got \(selectedCompany!) instead")
    }
    
    // MARK: - Helpers
    
    private func makeSUT(onCompanySelection: @escaping ((Company) -> Void) = { _ in }, file: StaticString = #file, line: UInt = #line) -> (sut: CompanySearchViewController, spy: LoaderSpy) {
        let spy = LoaderSpy()
        let sut = CompanySearchUIComposer.companySearchComposedWith(companySearcher: spy, headers: anySearchHeaders(), imageLoader: spy, onCompanySelection: onCompanySelection)
        trackForMemoryLeaks(spy, file: file, line: line)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, spy)
    }
    
    private func anySearchHeaders() -> SearchHeaders {
        SearchHeaders(accessToken: "token", client: "client", uid: "uid")
    }
    
    private func anyCompanies() -> [Company] {
        [
            Company(description: "a description", id: 1, name: "a name", photo: "photo/path"),
            Company(description: "another description", id: 2, name: "another name", photo: "another/photo/path"),
            Company(description: "other description", id: 3, name: "other name", photo: "other/photo/path")
        ]
    }
    
}
