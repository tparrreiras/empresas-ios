//
//  RubikFont.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

enum RubikWeight: String {
    case light
}

extension UIFont {
    
    static func rubikFont(ofSize size: CGFloat, weight: RubikWeight = .light) -> UIFont? {
        return UIFont(name: "Rubik-" + weight.rawValue.capitalized, size: size)
    }
    
}
