//
//  CompanyCell.swift
//  CompanySearchiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

import CompanySearch

final public class CompanyCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet private(set) public weak var imvBackground: UIImageView!
    @IBOutlet private(set) public weak var viewBlur: UIView!
    @IBOutlet private(set) public weak var lblTitle: UILabel!
    
    // MARK: - Property
    
    var model: Company? {
        didSet {
            self.lblTitle.text = self.model?.name.uppercased()
        }
    }
    
    // MARK: - Life Cycle
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        [self.imvBackground, self.viewBlur].forEach{ $0.layer.cornerRadius = 4 }
    }
    
    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.viewBlur.backgroundColor = UIColor.black.withAlphaComponent(selected ? 0.7 : 0.3)
    }
    
}
