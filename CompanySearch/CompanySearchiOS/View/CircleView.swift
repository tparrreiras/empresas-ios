//
//  CircleView.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

final class CircleView: UIView {
    var startAngle = 90
    var endAngle = 360
    
    init(frame: CGRect, startAngle: Int, endAngle: Int) {
        self.startAngle = startAngle
        self.endAngle = endAngle
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.backgroundColor = .clear
        let centerPoint = CGPoint(x: rect.width/2, y: rect.height/2)
        let path = UIBezierPath(arcCenter: centerPoint, radius: self.bounds.width/2, startAngle: self.startAngle.toRadians, endAngle: self.endAngle.toRadians, clockwise: true)
        
        self.layer.mask = nil
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
        
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.lineWidth = 3
        layer.strokeColor = #colorLiteral(red: 0.9843137255, green: 0.8588235294, blue: 0.9058823529, alpha: 1).cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.frame = self.bounds
        
        self.layer.addSublayer(layer)
    }
}

private extension Int {
    
    var toRadians: CGFloat {
        return CGFloat(self) * CGFloat.pi / 180
    }
    
}
