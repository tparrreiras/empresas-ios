//
//  SearchProgressView.swift
//  LoginiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

final public class SearchProgressView: UIView {
    
    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupUI()
    }

}

private extension SearchProgressView {
    
    // MARK: - Setup
    
    func setupUI() {
        self.backgroundColor = UIColor.white
        
        let innerCircle = CircleView(frame: .init(origin: .zero, size: CGSize(width: 48, height: 48)), startAngle: 90, endAngle: 360)
        innerCircle.center.x = self.center.x
        innerCircle.frame.origin.y = 132
        self.addSubview(innerCircle)
        
        let outerCircle = CircleView(frame: .init(origin: .zero, size: CGSize(width: 72, height: 72)), startAngle: 270, endAngle: 180)
        outerCircle.center.x = self.center.x
        outerCircle.frame.origin.y = 120
        self.addSubview(outerCircle)
        
        self.addAnimation(toLayer: innerCircle.layer, clockwise: true)
        self.addAnimation(toLayer: outerCircle.layer, clockwise: false)
    }
    
    func addAnimation(toLayer layer: CALayer, clockwise: Bool) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = clockwise ? 0.0 : CGFloat.pi * 2.0
        rotateAnimation.toValue = clockwise ? CGFloat.pi * 2.0 :  0.0
        rotateAnimation.duration = 2
        rotateAnimation.repeatCount = .infinity
        layer.add(rotateAnimation, forKey: "rotateAnimation")
    }
    
}

