//
//  CompanySearchViewController.swift
//  CompanySearchiOSTests
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

import CompanySearch

final public class CompanySearchViewController: UIViewController {
    
    // MARK: - IBOuetlets
    
    @IBOutlet private(set) public weak var imvHeader: UIImageView!
    @IBOutlet private(set) public weak var viewSearchBox: UIView!
    @IBOutlet private(set) public weak var txfSearch: UITextField!
    @IBOutlet private(set) public weak var tableView: UITableView!
    @IBOutlet private(set) public weak var lblError: UILabel!
    @IBOutlet private(set) public weak var constErrorCenterY: NSLayoutConstraint!
    @IBOutlet private(set) public weak var constHeaderTop: NSLayoutConstraint!
    
    // MARK: - IBActions
    
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        self.onLogoutAction?()
    }
    
    // MARK: - Properties
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private(set) public var progress: SearchProgressView?
    
    public var onLogoutAction: (() -> Void)?
    private let viewModel: CompanySearchViewModel
    private var tableModel = [Company]() {
        didSet { self.tableView.reloadData() }
    }
    private var onCompanySelection: ((Company) -> Void)
    private var tapGestureDismissKeyboard: UITapGestureRecognizer?
    
    
    // MARK: - Life Cycle
    
    public init(viewModel: CompanySearchViewModel, onCompanySelection: @escaping (Company) -> Void) {
        self.viewModel = viewModel
        self.onCompanySelection = onCompanySelection
        super.init(nibName: nil, bundle: Bundle(for: CompanySearchViewController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerKeyboardObservers()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeKeyboardObservers()
    }

}

private extension CompanySearchViewController {
    
    // MARK: - Observers
    
    func registerKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification), name: UIApplication.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification), name: UIApplication.keyboardWillHideNotification, object: nil)
    }

    func removeKeyboardObservers() {
        [UIApplication.keyboardWillShowNotification, UIApplication.keyboardWillHideNotification].forEach {
            NotificationCenter.default.removeObserver(self, name: $0, object: nil)
        }
    }
    
    // MARK: - Setup
    
    func setupUI() {
        self.setupDismissKeyboardTapGesture()
        self.setupTxfSearch()
        self.viewSearchBox.layer.cornerRadius = 4
        self.setupTable()
    }
    
    func setupDismissKeyboardTapGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
        self.tapGestureDismissKeyboard = gesture
    }
    
    func setupTxfSearch() {
        let font: UIFont = UIFont.rubikFont(ofSize: 18) ?? .systemFont(ofSize: 18, weight: .light)
        let color: UIColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        self.txfSearch.attributedPlaceholder = NSAttributedString(string: "Pesquise por empresa", attributes: [.font: font, .foregroundColor: color.withAlphaComponent(0.7)])
        self.txfSearch.font = font
        self.txfSearch.textColor = color
        self.txfSearch.delegate = self
        self.txfSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func setupTable() {
        self.tableView.keyboardDismissMode = .interactive
        self.tableView.isHidden = true
        self.tableView.separatorStyle = .none
        [CompanyCell.self, HeaderCell.self].forEach {
            self.tableView.register(UINib(nibName: String(describing: $0), bundle: Bundle(for: $0)), forCellReuseIdentifier: String(describing: $0))
        }
        self.tableView.prefetchDataSource = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // MARK: - Keyboard Presentation Handlers
    
    @objc func keyboardWillShowNotification(notification: Notification) {
        self.handleKeyboardPresentation(willPresent: true)
        if let userInfo = notification.userInfo {
            let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
            let keyboardHeight = keyboardRectangle?.height ?? 0
            self.updateErrorYPosition(with: -keyboardHeight/2)
            self.tableView.contentInset.bottom = keyboardHeight
        }
    }
    
    @objc func keyboardWillHideNotification() {
        self.handleKeyboardPresentation(willPresent: false)
        self.updateErrorYPosition(with: 0)
        self.tableView.contentInset.bottom = 0
    }
    
    func handleKeyboardPresentation(willPresent: Bool) {
        self.constHeaderTop.priority = .init(willPresent ? 250 : 999)
        if !willPresent {
            self.imvHeader.isHidden = false
        }
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { _ in
            if willPresent {
                self.imvHeader.isHidden = true
            }
        }
    }
    
    func updateErrorYPosition(with constant: CGFloat) {
        self.constErrorCenterY.constant = constant
        if !self.lblError.isHidden {
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    // MARK: - Progress
    
    func playProgress() {
        guard self.progress == nil else { return }
        let progress = SearchProgressView(frame: self.tableView.bounds)
        self.tableView.addSubview(progress)
        self.progress = progress
        self.tableView.isHidden = false
        self.lblError.isHidden = true
    }
    
    func stopProgress() {
        self.progress?.removeFromSuperview()
        self.progress = nil
    }
    
    // MARK: - General Methods
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.viewModel.searchCompany(with: textField.text ?? "")
    }
    
}

// MARK: - Company View

extension CompanySearchViewController: CompanyView {
    
    public func displayImageData(_ data: Data, for index: Int) {
        if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? CompanyCell, self.tableView.visibleCells.contains(cell) {
            cell.imvBackground.image = UIImage(data: data)
        }
    }
    
    public func displayLoading(isLoading: Bool) {
        isLoading ? self.playProgress() : self.stopProgress()
    }
    
    public func display(_ model: [Company]) {
        self.tableModel = model
        let hasModels = !model.isEmpty
        self.tableView.isHidden = !hasModels
        self.lblError.isHidden = hasModels
    }
    
}

// MARK: - TextField Delegate

extension CompanySearchViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.viewModel.searchCompany(with: textField.text ?? "")
        return textField.resignFirstResponder()
    }
    
}

// MARK: - Table View Data Source


extension CompanySearchViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableModel.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CompanyCell.self), for: indexPath) as! CompanyCell
        cell.model = tableModel[indexPath.row]
        let data = self.viewModel.imagesData[indexPath.row]
        if let data = data {
            cell.imvBackground.image = UIImage(data: data)
        } else {
            cell.imvBackground.image = nil
            self.viewModel.loadImage(at: indexPath.row)
        }
        return cell
    }
    
}

// MARK: - Table View Delegate

extension CompanySearchViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismissKeyboard()
        self.onCompanySelection(self.tableModel[indexPath.row])
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let text = self.viewModel.headerTitle
        let textHeight = NSAttributedString(string: text, attributes: [.font: UIFont.rubikFont(ofSize: 18)!])
        return textHeight.size().height + 28
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderCell.self)) as! HeaderCell
        cell.lblTitle.text = self.viewModel.headerTitle
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewModel.cancelImageLoad(at: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
    }
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// MARK: - Table View Data Source Prefetching

extension CompanySearchViewController: UITableViewDataSourcePrefetching {
    
    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { self.viewModel.loadImage(at: $0.row) }
    }
    
    public func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { self.viewModel.cancelImageLoad(at: $0.row) }
    }
    
}

// MARK: - Gesture Recognizer Delegate

extension CompanySearchViewController: UIGestureRecognizerDelegate {
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return gestureRecognizer == self.tapGestureDismissKeyboard ? self.tableModel.isEmpty : true
    }
    
}
