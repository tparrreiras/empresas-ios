//
//  CompanyDetailsViewController.swift
//  CompanySearchiOS
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import UIKit

import CompanySearch

final public class CompanyDetailsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imvHeader: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    // MARK: - Properties
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var viewModel: CompanyDetailsViewModel
    
    // MARK: - Life Cycle
    
    public init(viewModel: CompanyDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: Bundle(for: CompanyDetailsViewController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBackButton()
        self.setupViewModel()
        self.viewModel.loadImage()
    }

}

private extension CompanyDetailsViewController {
    
    // MARK: - Setup
    
    func setupBackButton() {
        let image = UIImage(named: "icon-back", in: Bundle(for: CompanyDetailsViewController.self), with: nil)?.withRenderingMode(.alwaysOriginal)
        let button = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = button
        self.navigationController?.navigationBar.backIndicatorImage = image
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = image
    }
    
    func setupViewModel() {
        self.lblName.text = self.viewModel.name
        self.lblMessage.text = self.viewModel.description
    }
    
}

// MARK: - Company Details View

extension CompanyDetailsViewController: CompanyDetailsView {
    
    public func displayImageData(_ data: Data) {
        self.imvHeader.image = UIImage(data: data)
    }
    
}
