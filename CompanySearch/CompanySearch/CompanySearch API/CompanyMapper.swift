//
//  CompanyMapper.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final class CompanyMapper {
    
    private struct Root: Decodable {
        let enterprises: [Item]
    }
    
    private struct Item: Decodable {
        let description: String
        let id: Int
        let name: String
        let photo: String
        
        var item: Company {
            return Company(description: description, id: id, name: name, photo: photo)
        }
        
        private enum CodingKeys: String, CodingKey {
            case description, id, photo
            case name = "enterprise_name"
        }
    }
    
    static var OK_200: Int { return 200}
    
    internal static func map(_ data: Data, _ response: HTTPURLResponse) throws -> [Company] {
        guard response.statusCode == OK_200, let root = try? JSONDecoder().decode(Root.self, from: data) else {
            throw try JSONDecoder().decode(CustomError.self, from: data)
        }
        return root.enterprises.map { $0.item }
    }
    
}
