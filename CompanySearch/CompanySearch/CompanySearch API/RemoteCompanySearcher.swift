//
//  RemoteCompanySearcher.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class RemoteCompanySearcher: CompanySearcher {
    
    private let url: URL
    private let client: HTTPClient
    
    public typealias SearchResult = CompanySearcher.SearchResult
    
    public enum Error: Swift.Error {
        case connectivity
        case invalidData
    }
    
    public init(url: URL, client: HTTPClient = URLSessionHTTPClient(session: .shared)) {
        self.url = url
        self.client = client
    }
    
    public func searchCompanies(for searchText: String, with headers: [String : String], completion: @escaping (SearchResult) -> Void) -> HTTPClientTask {
        let finalURL = URL(string: url.absoluteString + searchText)!
        return self.client.get(url: finalURL, withHeaders: headers) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case let .success((data, response)):
                completion(Result {
                    try CompanyMapper.map(data, response)
                })
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
}
