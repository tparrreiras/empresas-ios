//
//  SearchHeaders.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public struct SearchHeaders {
    public let accessToken: String
    public let client: String
    public let uid: String
    
    public init(
        accessToken: String,
        client: String,
        uid: String
    ){
        self.accessToken = accessToken
        self.client = client
        self.uid = uid
    }
}
