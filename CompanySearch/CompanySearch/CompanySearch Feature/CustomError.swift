//
//  CustomError.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public struct CustomError: Error, Decodable {
    public let errors: [String]
}
