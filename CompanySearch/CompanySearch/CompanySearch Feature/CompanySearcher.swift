//
//  CompanySearcher.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol CompanySearcher {
    typealias SearchResult = Swift.Result<[Company], Error>
    
    func searchCompanies(for searchText: String, with headers: [String: String], completion: @escaping (SearchResult) -> Void) -> HTTPClientTask
}
