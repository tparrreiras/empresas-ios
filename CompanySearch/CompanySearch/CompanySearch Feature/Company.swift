//
//  Company.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public struct Company: Equatable {
    public let description: String
    public let id: Int
    public let name: String
    public let photo: String
    
    public init(
        description: String,
        id: Int,
        name: String,
        photo: String
    ) {
        self.description = description
        self.id = id
        self.name = name
        self.photo = photo
    }
}
