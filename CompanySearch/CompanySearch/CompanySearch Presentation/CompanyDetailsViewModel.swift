//
//  CompanyDetailsViewModel.swift
//  CompanySearchiOS
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class CompanyDetailsViewModel {
    
    // MARK: - Properties
    
    weak public var companyDetailsView: CompanyDetailsView?
    
    public var name: String {
        return self.company.name.uppercased()
    }
    public var description: String {
        return self.company.description
    }
    
    private var company: Company
    private let imageLoader: ImageDataLoader
    private var imageTask: ImageDataLoaderTask?
    
    // MARK: - Life Cycle
    
    public init(company: Company, imageLoader: ImageDataLoader) {
        self.company = company
        self.imageLoader = imageLoader
    }
    
    deinit {
        self.imageTask?.cancel()
    }
    
    // MARK: - Load Image
    
    public func loadImage() {
        let url = URL(string: "https://empresas.ioasys.com.br" + company.photo)!
        self.imageTask = self.imageLoader.loadImageData(from: url) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(data):
                self.companyDetailsView?.displayImageData(data)
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
