//
//  CompanyView.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol CompanyView: class {
    func displayImageData(_ data: Data, for index: Int)
    func displayLoading(isLoading: Bool)
    func display(_ model: [Company])
}
