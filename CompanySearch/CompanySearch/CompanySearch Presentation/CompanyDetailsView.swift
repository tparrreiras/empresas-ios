//
//  CompanyDetailsView.swift
//  CompanySearch
//
//  Created by Tulio Parreiras on 11/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

public protocol CompanyDetailsView: class {
    func displayImageData(_ data: Data)
}
