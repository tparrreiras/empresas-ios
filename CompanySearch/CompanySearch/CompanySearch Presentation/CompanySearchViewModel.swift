//
//  CompanySearchViewModel.swift
//  CompanySearchiOS
//
//  Created by Tulio Parreiras on 10/09/20.
//  Copyright © 2020 Tulio Parreiras. All rights reserved.
//

import Foundation

final public class CompanySearchViewModel {
    
    // MARK: - Properties
    
    weak public var companyView: CompanyView?
    
    public var imagesData = [Int: Data]()
    public var headerTitle: String {
        let modelsCount = self.companies.count
        return String(format: "%2d", modelsCount) + (modelsCount == 1 ? " resultado encontrado" : " resultados encontrados")
    }
    
    private let companySearcher: CompanySearcher
    private let headers: SearchHeaders
    private let imageLoader: ImageDataLoader
    private var task: HTTPClientTask?
    private var companies = [Company]() {
        didSet {
            self.companyView?.display(self.companies)
            self.imageLoadTasks = [:]
            self.imagesData = [:]
        }
    }
    private var imageLoadTasks = [Int: ImageDataLoaderTask]()
    
    // MARK: - Life Cycle
    
    public init(companySearcher: CompanySearcher, headers: SearchHeaders, imageLoader: ImageDataLoader) {
        self.companySearcher = companySearcher
        self.headers = headers
        self.imageLoader = imageLoader
    }
    
    // MARK: - Search Company
    
    public func searchCompany(with searchText: String) {
        self.task?.cancel()
        let searchHeaders = [
            "access-token": headers.accessToken,
            "client": headers.client,
            "uid": headers.uid
        ]
        self.companyView?.displayLoading(isLoading: true)
        let _searchText = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? searchText
        self.task = self.companySearcher.searchCompanies(for: _searchText, with: searchHeaders) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(companies):
                self.companies = companies
                self.companyView?.displayLoading(isLoading: false)
            case let .failure(error):
                self.handleError(error)
            }
        }
    }
    
    private func handleError(_ error: Error) {
        if (error as NSError).code == NSURLErrorCancelled {
            // Do nothing, the view model cancel tasks when starting a new one
            print("Company search failed with connectivity error")
        } else {
            // ToDo: Handle errors, not mapped on design
            print("Company search failed with error: ", error)
            self.companyView?.displayLoading(isLoading: false)
        }
    }
    
    // MARK: - Image Load
    
    public func loadImage(at index: Int) {
        let company = self.companies[index]
        let url = URL(string: "https://empresas.ioasys.com.br" + company.photo)!
        self.imageLoadTasks[index] = self.imageLoader.loadImageData(from: url) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(data):
                self.imagesData[index] = data
                self.companyView?.displayImageData(data, for: index)
            case let .failure(error):
                print(error)
            }
        }
    }
    
    public func cancelImageLoad(at index: Int) {
        self.imageLoadTasks[index]?.cancel()
        self.imageLoadTasks[index] = nil
    }
    
}
